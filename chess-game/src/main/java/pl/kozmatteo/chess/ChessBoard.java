package pl.kozmatteo.chess;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.emptySet;

public class ChessBoard {
    private static final int BOARD_SIZE = 8;
    private final Map<Square, Piece> board;
    private final BoardNavigator boardNavigator = new BoardNavigator(BOARD_SIZE, BOARD_SIZE, this);

    public ChessBoard(Map<Square, Piece> board) {
        this.board = board;
    }

    public ChessBoard() {
        this.board = new HashMap<>();
        board.put(new Square("A8"), new Rook(Player.BLACK));
        board.put(new Square("B8"), new Knight(Player.BLACK));
        board.put(new Square("C8"), new Bishop(Player.BLACK));
        board.put(new Square("D8"), new King(Player.BLACK));
        board.put(new Square("E8"), new Queen(Player.BLACK));
        board.put(new Square("F8"), new Bishop(Player.BLACK));
        board.put(new Square("G8"), new Knight(Player.BLACK));
        board.put(new Square("H8"), new Rook(Player.BLACK));

        board.put(new Square("A7"), createBlackPawn());
        board.put(new Square("B7"), createBlackPawn());
        board.put(new Square("C7"), createBlackPawn());
        board.put(new Square("D7"), createBlackPawn());
        board.put(new Square("E7"), createBlackPawn());
        board.put(new Square("F7"), createBlackPawn());
        board.put(new Square("G7"), createBlackPawn());
        board.put(new Square("H7"), createBlackPawn());

        board.put(new Square("A2"), createWhitePawn());
        board.put(new Square("B2"), createWhitePawn());
        board.put(new Square("C2"), createWhitePawn());
        board.put(new Square("D2"), createWhitePawn());
        board.put(new Square("E2"), createWhitePawn());
        board.put(new Square("F2"), createWhitePawn());
        board.put(new Square("G2"), createWhitePawn());
        board.put(new Square("H2"), createWhitePawn());

        board.put(new Square("A1"), new Rook(Player.WHITE));
        board.put(new Square("B1"), new Knight(Player.WHITE));
        board.put(new Square("C1"), new Bishop(Player.WHITE));
        board.put(new Square("D1"), new Queen(Player.WHITE));
        board.put(new Square("E1"), new King(Player.WHITE));
        board.put(new Square("F1"), new Bishop(Player.WHITE));
        board.put(new Square("G1"), new Knight(Player.WHITE));
        board.put(new Square("H1"), new Rook(Player.WHITE));
    }

    private Piece createWhitePawn() {
        return new Pawn(Player.WHITE);
    }

    private Piece createBlackPawn() {
        return new Pawn(Player.BLACK);
    }

    public Optional<PieceView> getPieceAt(Square coordinate) {
        return Optional.ofNullable(board.get(coordinate));
    }

    public void move(Square from, Square to) {
        validate(from, to);
        final var piece = board.get(from);
        board.put(to, piece);
        board.put(from, null);
        piece.markAsMoved();
    }

    private void validate(Square from, Square to) {
        final var piece = board.get(from);
        if (piece == null) {
            throw new IllegalArgumentException("Cannot make a move from an empty square!");
        }
        if (!isValidMove(piece, from, to)) {
            throw new IllegalArgumentException("Invalid move for a piece");
        }
    }

    private boolean isValidMove(Piece piece, Square from, Square to) {
        return piece.getAvailableMovesFrom(from, boardNavigator).contains(to);
    }

    public Set<Square> getAvailableMovesFrom(Square from) {
        return getPieceAt(from)
                .map(pv -> ((Piece) pv).getAvailableMovesFrom(from, boardNavigator))
                .orElse(noMoves());
    }

    private Set<Square> noMoves() {
        return emptySet();
    }
}
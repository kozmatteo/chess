package pl.kozmatteo.chess;

public interface ChessGameListener {
    void onMove(Square from, Square to);
}

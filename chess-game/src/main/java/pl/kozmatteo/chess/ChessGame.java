package pl.kozmatteo.chess;

import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
public class ChessGame {
    private final ChessBoard chessBoard;
    private final ChessGameListener chessGameListener;

    private Player currentPlayer = Player.WHITE;

    public Optional<PieceView> getPieceAt(Square coordinate) {
        return chessBoard.getPieceAt(coordinate);
    }

    public boolean isTurnToMove(PieceView piece) {
        return piece.getPlayer() == currentPlayer;
    }

    public void move(Square from, Square to) {
        if (!isPieceTurn(from)) {
            throw new IllegalStateException("Cannot move piece. It's not your turn");
        }
        chessBoard.move(from, to);
        chessGameListener.onMove(from, to);
        nextPlayer();
    }

    private boolean isPieceTurn(Square from) {
        return chessBoard.getPieceAt(from).filter(this::isTurnToMove).isPresent();
    }

    private void nextPlayer() {
        currentPlayer = currentPlayer == Player.WHITE ? Player.BLACK : Player.WHITE;
    }

    public Set<Square> getAvailableMovesFrom(Square from) {
        return chessBoard.getAvailableMovesFrom(from);
    }
}

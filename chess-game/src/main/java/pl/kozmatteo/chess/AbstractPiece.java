package pl.kozmatteo.chess;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public abstract class AbstractPiece implements Piece {
    protected final Player player;
    protected final String symbol;
    protected boolean hasMoved;

    @Override
    public abstract Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator);

    @Override
    public final Player getPlayer() {
        return player;
    }

    @Override
    public final String getSymbol() {
        return symbol;
    }

    @Override
    public void markAsMoved() {
        hasMoved = true;
    }
}

package pl.kozmatteo.chess;

import java.util.HashSet;
import java.util.Set;

public class Queen extends AbstractPiece {
    private static final String QUEEN_SYMBOL = "Q";

    public Queen(Player player) {
        super(player, QUEEN_SYMBOL);
    }

    @Override
    public Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator) {
        Set<Square> availableMoves = new HashSet<>();
        availableMoves.addAll(boardNavigator.getHorizontalMoves(coordinate, player));
        availableMoves.addAll(boardNavigator.getVerticalMoves(coordinate, player));
        availableMoves.addAll(boardNavigator.getDiagonalMoves(coordinate, player));
        return availableMoves;
    }
}

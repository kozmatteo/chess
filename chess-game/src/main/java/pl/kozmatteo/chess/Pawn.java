package pl.kozmatteo.chess;

import java.util.Set;

public class Pawn extends AbstractPiece {
    private static final String PAWN_SYMBOL = "P";
    private final Direction direction;

    public Pawn(Player player) {
        super(player, PAWN_SYMBOL);
        this.direction = player == Player.WHITE ? Direction.UP : Direction.DOWN;
    }

    public Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator) {
        if (!hasMoved) {
            return direction.getMovesForWithinTwoSquares(boardNavigator, coordinate, player);
        }
        return direction.getMovesForWithinOneSquares(boardNavigator, coordinate, player);
    }

    private enum Direction {
        UP {
            @Override
            Set<Square> getMovesForWithinTwoSquares(BoardNavigator navigator, Square square, Player player) {
                return navigator.getMovesUp(square, player, 2);
            }

            @Override
            Set<Square> getMovesForWithinOneSquares(BoardNavigator navigator, Square square, Player player) {
                return navigator.getMovesUp(square, player, 1);
            }
        }, DOWN {
            @Override
            Set<Square> getMovesForWithinTwoSquares(BoardNavigator navigator, Square square, Player player) {
                return navigator.getMovesDown(square, player, 2);
            }

            @Override
            Set<Square> getMovesForWithinOneSquares(BoardNavigator navigator, Square square, Player player) {
                return navigator.getMovesDown(square, player, 1);
            }
        };

        abstract Set<Square> getMovesForWithinTwoSquares(BoardNavigator navigator, Square square, Player player);

        abstract Set<Square> getMovesForWithinOneSquares(BoardNavigator navigator, Square square, Player player);
    }
}

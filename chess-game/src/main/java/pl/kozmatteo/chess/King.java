package pl.kozmatteo.chess;

import java.util.HashSet;
import java.util.Set;

public class King extends AbstractPiece {
    private static final int MOVEMENT_RANGE = 1;
    private static final String KING_SYMBOL = "K";

    public King(Player player) {
        super(player, KING_SYMBOL);
    }

    @Override
    public Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator) {
        Set<Square> availableMoves = new HashSet<>();
        availableMoves.addAll(boardNavigator.getHorizontalMoves(coordinate, player, MOVEMENT_RANGE));
        availableMoves.addAll(boardNavigator.getVerticalMoves(coordinate, player, MOVEMENT_RANGE));
        availableMoves.addAll(boardNavigator.getDiagonalMoves(coordinate, player, MOVEMENT_RANGE));
        return availableMoves;
    }
}

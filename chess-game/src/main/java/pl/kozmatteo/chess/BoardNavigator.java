package pl.kozmatteo.chess;

import lombok.RequiredArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiFunction;

@RequiredArgsConstructor
public class BoardNavigator {
    private static final BiFunction<Square, Integer, Square> UP = (square, distance) -> new Square(square.getColumnLetter(), square.getRow() + distance);
    private static final BiFunction<Square, Integer, Square> DOWN = (square, distance) -> new Square(square.getColumnLetter(), square.getRow() - distance);
    private static final BiFunction<Square, Integer, Square> LEFT = (square, distance) -> new Square((char) (square.getColumnLetter() - distance), square.getRow());
    private static final BiFunction<Square, Integer, Square> RIGHT = (square, distance) -> new Square((char) (square.getColumnLetter() + distance), square.getRow());
    private static final BiFunction<Square, Integer, Square> UPPER_RIGHT = (sq, i) -> new Square((char) (sq.getColumnLetter() + i), sq.getRow() + i);
    private static final BiFunction<Square, Integer, Square> BOTTOM_RIGHT = (sq, i) -> new Square((char) (sq.getColumnLetter() + i), sq.getRow() - i);
    private static final BiFunction<Square, Integer, Square> UPPER_LEFT = (sq, i) -> new Square((char) (sq.getColumnLetter() - i), sq.getRow() + i);
    private static final BiFunction<Square, Integer, Square> BOTTOM_LEFT = (sq, i) -> new Square((char) (sq.getColumnLetter() - i), sq.getRow() - i);

    private static final char FIRST_COLUMN = 'a';
    private static final int FIRST_ROW = 1;
    private final int height;
    private final int width;
    private final ChessBoard chessBoard;

    public Set<Square> getMovesDown(Square from, Player player, int range) {
        return movesFrom(from, player, range, DOWN);
    }

    public Set<Square> getMovesUp(Square from, Player player, int range) {
        return movesFrom(from, player, range, UP);
    }

    public Set<Square> getMovesUp(Square from, Player player) {
        return getMovesUp(from, player, height);
    }

    public Set<Square> getMovesDown(Square from, Player player) {
        return getMovesDown(from, player, width);
    }

    public Set<Square> getVerticalMoves(Square from, Player player) {
        return getVerticalMoves(from, player, height);
    }

    public Set<Square> getVerticalMoves(Square from, Player player, int range) {
        final var squaresSet = new HashSet<Square>();
        squaresSet.addAll(getMovesUp(from, player, range));
        squaresSet.addAll(getMovesDown(from, player, range));
        return squaresSet;
    }

    public Set<Square> getHorizontalMoves(Square from, Player player) {
        return getHorizontalMoves(from, player, width);
    }

    public Set<Square> getHorizontalMoves(Square from, Player player, int range) {
        Set<Square> result = new HashSet<>();
        result.addAll(movesFrom(from, player, range, LEFT));
        result.addAll(movesFrom(from, player, range, RIGHT));
        return result;
    }

    private int getLastAllowedColumnOffset() {
        return FIRST_COLUMN + width - 1;
    }

    public Set<Square> getDiagonalMoves(Square from, Player player) {
        return getDiagonalMoves(from, player, width);
    }

    public Set<Square> getDiagonalMoves(Square from, Player player, int range) {
        Set<Square> result = new HashSet<>();
        result.addAll(movesFrom(from, player, range, UPPER_RIGHT));
        result.addAll(movesFrom(from, player, range, BOTTOM_RIGHT));
        result.addAll(movesFrom(from, player, range, UPPER_LEFT));
        result.addAll(movesFrom(from, player, range, BOTTOM_LEFT));
        return result;
    }

    private Set<Square> movesFrom(Square from, Player player, int range, BiFunction<Square, Integer, Square> direction) {
        Set<Square> result = new HashSet<>();
        for (int distance = 1; distance <= Math.min(range, width); distance++) {
            final Square nextSquare = direction.apply(from, distance);
            if (isValidSquare(nextSquare)) {
                final var collidingSquare = chessBoard.getPieceAt(nextSquare)
                                                      .map(CollidingSquare::of)
                                                      .orElseGet(CollidingSquare::nonColliding);
                if (!collidingSquare.isColliding()) {
                    result.add(nextSquare);
                } else if (collidingSquare.isTakenBy(player)) {
                    break;
                } else {
                    result.add(nextSquare);
                    break;
                }
            }
        }
        return result;
    }

    private boolean isValidSquare(Square square) {
        return isValidSquare(square.getColumnLetter(), square.getRow());
    }

    private boolean isValidSquare(char column, int row) {
        return row <= height && row >= FIRST_ROW && column >= FIRST_COLUMN && column <= getLastAllowedColumnOffset();
    }

    public boolean isValidMove(Square square, Player player) {
        if (!isValidSquare(square)) {
            return false;
        }
        final var canMove = true;
        return chessBoard.getPieceAt(square)
                         .map(CollidingSquare::of)
                         .map(cs -> !cs.isTakenBy(player))
                         .orElse(canMove);
    }

    @RequiredArgsConstructor
    private static class CollidingSquare {
        private final PieceView pieceView;

        static CollidingSquare of(PieceView view) {
            return new CollidingSquare(view);
        }

        static CollidingSquare nonColliding() {
            return new CollidingSquare(null);
        }

        public boolean isColliding() {
            return pieceView != null;
        }

        public boolean isTakenBy(Player player) {
            return pieceView != null && pieceView.getPlayer().equals(player);
        }

    }

}

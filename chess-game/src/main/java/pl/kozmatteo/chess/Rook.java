package pl.kozmatteo.chess;

import java.util.HashSet;
import java.util.Set;

public class Rook extends AbstractPiece {
    private static final String ROOK_SYMBOL = "R";

    public Rook(Player player) {
        super(player, ROOK_SYMBOL);
    }

    @Override
    public Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator) {
        Set<Square> availableMoves = new HashSet<>();
        availableMoves.addAll(boardNavigator.getHorizontalMoves(coordinate, player));
        availableMoves.addAll(boardNavigator.getVerticalMoves(coordinate, player));
        return availableMoves;
    }
}

package pl.kozmatteo.chess;

import java.util.Set;

public class Bishop extends AbstractPiece {
    private static final String BISHOP_SYMBOL = "B";

    public Bishop(Player player) {
        super(player, BISHOP_SYMBOL);
    }

    @Override
    public Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator) {
        return boardNavigator.getDiagonalMoves(coordinate, player);
    }
}

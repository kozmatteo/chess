package pl.kozmatteo.chess;

import java.util.Set;

public interface Piece extends PieceView {
    Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator);

    default void markAsMoved() {}
}

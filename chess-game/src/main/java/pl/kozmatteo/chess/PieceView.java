package pl.kozmatteo.chess;

public interface PieceView {
    Player getPlayer();

    String getSymbol();
}

package pl.kozmatteo.chess;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class Knight extends AbstractPiece {
    private static final String KNIGHT_SYMBOL = "Kn";

    public Knight(Player player) {
        super(player, KNIGHT_SYMBOL);
    }

    @Override
    public Set<Square> getAvailableMovesFrom(Square coordinate, BoardNavigator boardNavigator) {
        final var column = coordinate.getColumnLetter();
        final var previousColumn = (char) (column - 1);
        final var previous2Column = (char) (column - 2);
        final var nextColumn = (char) (column + 1);
        final var next2Column = (char) (column + 2);
        var up2 = coordinate.getRow() + 2;
        var up1 = coordinate.getRow() + 1;
        var down1 = coordinate.getRow() - 1;
        var down2 = coordinate.getRow() - 2;
        Set<Square> candidateMoves = Set.of(
                new Square(previousColumn, up2),
                new Square(previousColumn, down2),
                new Square(nextColumn, up2),
                new Square(nextColumn, down2),
                new Square(next2Column, up1),
                new Square(next2Column, down1),
                new Square(previous2Column, up1),
                new Square(previous2Column, down1)
        );
        return candidateMoves.stream().filter(square -> boardNavigator.isValidMove(square, player)).collect(toSet());
    }
}

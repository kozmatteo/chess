package pl.kozmatteo.chess;

import lombok.Data;

import java.util.regex.Pattern;

@Data
public final class Square {
    private static final Pattern validPositions = Pattern.compile("^[a-zA-Z][1-8]$");
    private final char columnLetter;
    private final int row;

    public Square(String line) {
        this(line.toLowerCase().charAt(0), Integer.parseInt(line.substring(1)));
    }

    Square(char columnLetter, int row) {
        this.columnLetter = columnLetter;
        this.row = row;
    }

    @Override
    public String toString() {
        return String.valueOf(columnLetter) + row;
    }
}

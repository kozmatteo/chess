package pl.kozmatteo.chess;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class SquareFixture {

    public static Square validFieldA() {
        return new Square("A2");
    }

    public static Square validFieldB() {
        return new Square("A3");
    }

    public static Square validFieldC() {
        return new Square("A4");
    }

    public static Square validFieldD() {
        return new Square("A5");
    }

    @SafeVarargs
    public static Set<Square> merge(Set<Square>... squares) {
        final var builder = Stream.<Set<Square>>builder();
        for (Set<Square> square : squares) {
            builder.add(square);
        }
        return builder.build()
                      .flatMap(Collection::stream)
                      .collect(toSet());
    }
}

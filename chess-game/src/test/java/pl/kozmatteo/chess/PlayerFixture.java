package pl.kozmatteo.chess;

public class PlayerFixture {
    private PlayerFixture() {
    }

    public static final Player A_PLAYER = Player.WHITE;
    public static final Player AN_OPPONENT = Player.BLACK;

}
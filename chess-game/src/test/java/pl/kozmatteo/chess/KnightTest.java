package pl.kozmatteo.chess;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class KnightTest {

    private Knight knight;

    @BeforeEach
    void setUp() {
        knight = aKnight();
    }

    @Test
    @DisplayName("knight moves in L shaped fashion")
    void knightMoves() {
        BoardNavigator boardNavigator = mock(BoardNavigator.class);
        given(boardNavigator.isValidMove(new Square("D6"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("C5"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("F6"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("G5"), knight.getPlayer())).willReturn(true);

        given(boardNavigator.isValidMove(new Square("D2"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("C3"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("F2"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("G3"), knight.getPlayer())).willReturn(true);

        final var knightMovesSet = knight.getAvailableMovesFrom(new Square("E4"), boardNavigator);

        BDDAssertions.then(knightMovesSet)
                     .containsExactlyInAnyOrder(new Square("D6"),
                             new Square("C5"),
                             new Square("F6"),
                             new Square("G5"),
                             new Square("D2"),
                             new Square("C3"),
                             new Square("F2"),
                             new Square("G3"));
    }

    @Test
    @DisplayName("if some knight moves are restricted are filter out by board navigator")
    void knightMovesBorderChecks() {
        BoardNavigator boardNavigator = mock(BoardNavigator.class);
        given(boardNavigator.isValidMove(new Square("D6"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("C5"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("F6"), knight.getPlayer())).willReturn(true);
        given(boardNavigator.isValidMove(new Square("G5"), knight.getPlayer())).willReturn(true);

        final var knightMovesSet = knight.getAvailableMovesFrom(new Square("E4"), boardNavigator);

        BDDAssertions.then(knightMovesSet)
                     .containsExactlyInAnyOrder(new Square("D6"),
                             new Square("C5"),
                             new Square("F6"),
                             new Square("G5"))
                     .hasSize(4);
    }

    private Knight aKnight() {
        return new Knight(Player.BLACK);
    }
}
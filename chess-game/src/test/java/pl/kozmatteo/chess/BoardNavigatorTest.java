package pl.kozmatteo.chess;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static pl.kozmatteo.chess.PlayerFixture.AN_OPPONENT;
import static pl.kozmatteo.chess.PlayerFixture.A_PLAYER;

@DisplayName("given a board navigator for board of size 8x8")
class BoardNavigatorTest {
    private final int boardSize = 8;
    private ChessBoard chessBoard = mock(ChessBoard.class);
    private BoardNavigator boardNavigator = new BoardNavigator(boardSize, boardSize, chessBoard);
    private Square bottomEndSquare = new Square("A1");
    private Square topEndSquare = new Square("A8");
    private Square middleSquare = new Square("A4");

    @Nested
    @DisplayName("display moves up")
    class MovesUp {
        private Square middleSquareOneUp = new Square("A5");
        private Set<Square> squaresFromMiddleSquareToTopBorder = Set.of(middleSquareOneUp, new Square("A6"), new Square("A7"), new Square("A8"));

        @Test
        @DisplayName("get moves up on the edge of the board")
        void upFromTheEdge() {
            final var movesSet = boardNavigator.getMovesUp(topEndSquare, A_PLAYER, 1);

            then(movesSet).isEmpty();
        }

        @Test
        @DisplayName("get one square up from the middle of the board")
        void upFromAMiddle() {
            final var movesSet = boardNavigator.getMovesUp(middleSquare, A_PLAYER, 1);

            then(movesSet).contains(middleSquareOneUp);
        }

        @Test
        @DisplayName("get moves up to the board border")
        void upFromAMiddleTilBorder() {
            final var movesSet = boardNavigator.getMovesUp(middleSquare, A_PLAYER);

            then(movesSet).containsExactlyInAnyOrderElementsOf(squaresFromMiddleSquareToTopBorder);
        }
    }

    @Nested
    @DisplayName("display moves down")
    class MovesDown {
        private Square middleSquareOneDown = new Square("A3");
        private Set<Square> squaresFromMiddleSquareToBottomBorder = Set.of(middleSquareOneDown, new Square("A2"), new Square("A1"));

        @Test
        @DisplayName("get moves down on the edge of the board")
        void downFromTheEdge() {
            final var movesSet = boardNavigator.getMovesDown(bottomEndSquare, A_PLAYER, 1);

            then(movesSet).isEmpty();
        }

        @Test
        @DisplayName("get one square down from the middle of the board")
        void downFromAMiddle() {
            final var movesSet = boardNavigator.getMovesDown(middleSquare, A_PLAYER, 1);

            then(movesSet).contains(middleSquareOneDown);
        }

        @Test
        @DisplayName("get moves down to the board border")
        void downFromAMiddleTilBorder() {
            final var movesSet = boardNavigator.getMovesDown(middleSquare, A_PLAYER);

            then(movesSet).containsExactlyInAnyOrderElementsOf(squaresFromMiddleSquareToBottomBorder);
        }
    }

    @Nested
    @DisplayName("vertical moves")
    class VerticalMoves {
        private Set<Square> verticalSquares = Set.of(new Square("A5"), new Square("A6"), new Square("A7"), new Square("A8"), new Square("A3"), new Square("A2"), new Square("A1"));

        @Test
        @DisplayName("should return all moves vertically within the board")
        void getVerticalMoves() {
            final var movesSet = boardNavigator.getVerticalMoves(middleSquare, A_PLAYER);

            then(movesSet).containsExactlyInAnyOrderElementsOf(verticalSquares);
        }
    }


    @Nested
    @DisplayName("horizontal moves")
    class HorizontalMoves {
        private Set<Square> horizontalSquares = Set.of(
                new Square("a4"), new Square("B4"), new Square("C4"), new Square("d4"), new Square("f4"), new Square("g4"), new Square("h4")
        );

        @Test
        @DisplayName("should return all moves horizontally within the board")
        void getHorizontalMoves() {
            final var movesSet = boardNavigator.getHorizontalMoves(new Square("e4"), A_PLAYER);

            then(movesSet).containsExactlyInAnyOrderElementsOf(horizontalSquares);
        }
    }

    @Nested
    @DisplayName("diagonal moves")
    class DiagonalMoves {
        private Set<Square> diagonalSquares = Set.of(
                // bottom-left
                new Square("d3"), new Square("c2"), new Square("b1"),
                // upper left
                new Square("d5"), new Square("c6"), new Square("b7"), new Square("a8"),
                // upper right
                new Square("f5"), new Square("g6"), new Square("h7"),
                // bottom left
                new Square("f3"), new Square("g2"), new Square("h1")
        );

        @Test
        @DisplayName("should return all moves diagonally within the board")
        void getDiagonalMoves() {
            final var movesSet = boardNavigator.getDiagonalMoves(new Square("E4"), A_PLAYER);

            then(movesSet).containsExactlyInAnyOrderElementsOf(diagonalSquares);
        }
    }

    @Nested
    @DisplayName("collision awareness")
    class CollisionDetector {
        private Optional<PieceView> anOpponentsPiece = Optional.of(new TestPieceView(AN_OPPONENT));
        private Optional<PieceView> aPlayersPiece = Optional.of(new TestPieceView(A_PLAYER));

        @Nested
        @DisplayName("moving up")
        class MovingUp {
            @Test
            @DisplayName("when in a movement direction there's a piece of the same color filter-out squares from there")
            void cannotMovePastPieceOFameColor() {
                given(chessBoard.getPieceAt(new Square("A4"))).willReturn(aPlayersPiece);

                final var movesSet = boardNavigator.getMovesUp(new Square("A2"), A_PLAYER);

                then(movesSet).containsExactly(new Square("A3"));
            }

            @Test
            @DisplayName("when in a movement can get at most to square of a opponent piece")
            void canMoveUpToOpponentField() {
                given(chessBoard.getPieceAt(new Square("A4"))).willReturn(anOpponentsPiece);

                final var movesSet = boardNavigator.getMovesUp(new Square("A2"), A_PLAYER);

                then(movesSet).containsExactlyInAnyOrder(new Square("A3"), new Square("A4"));
            }
        }

        @Nested
        @DisplayName("moving down")
        class MovingDown {
            @Test
            @DisplayName("when in a movement direction there's a piece of the same color filter-out squares from there")
            void cannotMovePastPieceOFameColor() {
                given(chessBoard.getPieceAt(new Square("A4"))).willReturn(aPlayersPiece);

                final var movesSet = boardNavigator.getMovesDown(new Square("A6"), A_PLAYER);

                then(movesSet).containsExactly(new Square("A5"));
            }

            @Test
            @DisplayName("when in a movement can get at most to square of a opponent piece")
            void canMoveDownToOpponentField() {
                given(chessBoard.getPieceAt(new Square("A4"))).willReturn(anOpponentsPiece);

                final var movesSet = boardNavigator.getMovesDown(new Square("A6"), A_PLAYER);

                then(movesSet).containsExactlyInAnyOrder(new Square("A5"), new Square("A4"));
            }
        }


        @Nested
        @DisplayName("moving diagonally")
        class MovingDiagonal {
            @Test
            @DisplayName("when in a movement direction there's a piece of the same color filter-out squares from there")
            void cannotMovePastPieceOFameColor() {
                given(chessBoard.getPieceAt(new Square("G7"))).willReturn(anOpponentsPiece);
                given(chessBoard.getPieceAt(new Square("C7"))).willReturn(anOpponentsPiece);
                given(chessBoard.getPieceAt(new Square("G3"))).willReturn(aPlayersPiece);
                given(chessBoard.getPieceAt(new Square("C3"))).willReturn(aPlayersPiece);

                final var movesSet = boardNavigator.getDiagonalMoves(new Square("E5"), A_PLAYER);

                then(movesSet).containsExactlyInAnyOrder(new Square("D4"), new Square("D6"), new Square("C7"), new Square("F6"), new Square("G7"), new Square("F4"));
            }
        }

        @RequiredArgsConstructor
        class TestPieceView implements PieceView {
            private final Player player;

            @Override
            public Player getPlayer() {
                return player;
            }

            @Override
            public String getSymbol() {
                return null;
            }
        }
    }
}
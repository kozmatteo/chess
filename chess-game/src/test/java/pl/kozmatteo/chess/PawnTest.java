package pl.kozmatteo.chess;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class PawnTest {

    private final Square baseField = new Square("A4");
    private final Square oneSquareUp = new Square("A5");
    private final Square twoSquaresUp = new Square("A6");
    private final Square oneSquareDown = new Square("A3");
    private final Square twoSquaresDown = new Square("A2");
    private final Set<Square> noMoves = emptySet();

    BoardNavigator boardNavigator = mock(BoardNavigator.class);

    @Nested
    @DisplayName("given white pawn")
    class WhitePawnTest {
        private final Player white = Player.WHITE;
        private Pawn whitePawn = new Pawn(white);

        @Test
        @DisplayName("can move one either two squares forward")
        void whitePawnForwardOneSquare() {
            given(boardNavigator.getMovesUp(baseField, white, 2)).willReturn(Set.of(oneSquareUp, twoSquaresUp));

            final var moves = whitePawn.getAvailableMovesFrom(baseField, boardNavigator);

            then(moves)
                    .containsOnly(oneSquareUp, twoSquaresUp);
        }

        @Nested
        @DisplayName("and has already been moved")
        class HasAlreadyBeenMoved {
            {
                whitePawn.markAsMoved();
            }

            @DisplayName("when moved then can only move by one")
            @Test
            void pawnCanOnlyMakeMoveByOne() {
                given(boardNavigator.getMovesUp(baseField, white, 1)).willReturn(Set.of(oneSquareUp));

                final var moves = whitePawn.getAvailableMovesFrom(baseField, boardNavigator);

                then(moves)
                        .containsOnly(oneSquareUp);
            }
        }
    }

    @Nested
    @DisplayName("given black pawn")
    class BlackPawnTest {
        private final Player black = Player.BLACK;
        private Pawn blackPawn = new Pawn(black);

        @Test
        @DisplayName("can move one either two squares forward")
        void blackPawnForwardOneSquare() {
            given(boardNavigator.getMovesDown(baseField, black, 2)).willReturn(Set.of(oneSquareDown, twoSquaresDown));

            final var moves = blackPawn.getAvailableMovesFrom(baseField, boardNavigator);

            then(moves)
                    .containsOnly(oneSquareDown, twoSquaresDown);
        }

        @Nested
        @DisplayName("and has already been moved")
        class HasAlreadyBeenMoved {
            {
                blackPawn.markAsMoved();
            }

            @DisplayName("when moved then can only move by one")
            @Test
            void pawnCanOnlyMakeMoveByOne() {
                given(boardNavigator.getMovesDown(baseField, black, 1)).willReturn(Set.of(oneSquareDown));

                final var moves = blackPawn.getAvailableMovesFrom(baseField, boardNavigator);

                then(moves)
                        .containsOnly(oneSquareDown);
            }
        }

        @Nested
        @DisplayName("when is at the end of the board")
        class EndOfTheBoard {
            private Square endOfTheBoard = new Square("A1");

            @Test
            @DisplayName("then cannot move any farther")
            void cannotMoveFurther() {
                given(boardNavigator.getMovesDown(any(Square.class), eq(black), anyInt())).willReturn(noMoves);

                final var moves = blackPawn.getAvailableMovesFrom(endOfTheBoard, boardNavigator);

                then(moves).isEmpty();
            }
        }
    }
}
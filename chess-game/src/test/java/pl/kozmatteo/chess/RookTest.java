package pl.kozmatteo.chess;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static pl.kozmatteo.chess.PlayerFixture.A_PLAYER;
import static pl.kozmatteo.chess.SquareFixture.merge;
import static pl.kozmatteo.chess.SquareFixture.validFieldA;

class RookTest {

    private BoardNavigator boardNavigator = mock(BoardNavigator.class);

    @Test
    @DisplayName("rook can move vertically and horizontally")
    void movement() {
        // given
        var rook = aRook();
        final var verticalMovesSet = Set.of(new Square("A1"), new Square("B2"));
        given(boardNavigator.getVerticalMoves(validFieldA(), A_PLAYER)).willReturn(verticalMovesSet);
        final var horizontalMovesSet = Set.of(new Square("X1"), new Square("Y2"));
        given(boardNavigator.getHorizontalMoves(validFieldA(), A_PLAYER)).willReturn(horizontalMovesSet);

        // when
        final var availableMoves = rook.getAvailableMovesFrom(validFieldA(), boardNavigator);

        then(availableMoves)
                .hasSize(verticalMovesSet.size() + horizontalMovesSet.size())
                .containsAll(merge(horizontalMovesSet, verticalMovesSet));
    }

    private Rook aRook() {
        return new Rook(A_PLAYER);
    }
}
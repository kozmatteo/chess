package pl.kozmatteo.chess;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static pl.kozmatteo.chess.SquareFixture.validFieldA;
import static pl.kozmatteo.chess.SquareFixture.validFieldB;
import static pl.kozmatteo.chess.SquareFixture.validFieldC;
import static pl.kozmatteo.chess.SquareFixture.validFieldD;

class KingTest {

    @Test
    @DisplayName("king can move all around within a range of one filed")
    void kingMoves() {
        BoardNavigator boardNavigator = mock(BoardNavigator.class);
        given(boardNavigator.getHorizontalMoves(validFieldA(), aKing().getPlayer(), 1)).willReturn(Set.of(validFieldB()));
        given(boardNavigator.getVerticalMoves(validFieldA(), aKing().getPlayer(), 1)).willReturn(Set.of(validFieldC()));
        given(boardNavigator.getDiagonalMoves(validFieldA(), aKing().getPlayer(), 1)).willReturn(Set.of(validFieldD()));

        var availableMoves = aKing().getAvailableMovesFrom(validFieldA(), boardNavigator);

        BDDAssertions.then(availableMoves)
                     .containsOnly(validFieldB(), validFieldC(), validFieldD());
    }

    private King aKing() {
        return new King(Player.BLACK);
    }
}
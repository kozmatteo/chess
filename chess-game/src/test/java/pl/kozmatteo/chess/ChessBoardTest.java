package pl.kozmatteo.chess;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static pl.kozmatteo.chess.SquareFixture.validFieldA;
import static pl.kozmatteo.chess.SquareFixture.validFieldB;
import static pl.kozmatteo.chess.SquareFixture.validFieldC;

class ChessBoardTest {

    private final Set<Square> noMoves = emptySet();
    private final Piece piece = mock(Piece.class);
    private final ChessBoard board = new ChessBoard(new HashMap<>(Map.of(validFieldA(), piece)));
    private final Square emptyField = validFieldC();

    @Test
    @DisplayName("moves a piece")
    void move() {
        // given
        given(piece.getAvailableMovesFrom(eq(validFieldA()), any(BoardNavigator.class))).willReturn(Set.of(validFieldB(), validFieldC()));

        // when
        board.move(validFieldA(), validFieldB());

        // then
        assertThat(board.getPieceAt(validFieldB())).containsSame(piece);
        assertThat(board.getPieceAt(validFieldA())).isEmpty();
        then(piece).should(times(1)).markAsMoved();
    }

    @Test
    @DisplayName("cannot move a piece: invalid move for a piece")
    void cannotMovePiece() {
        // given
        given(piece.getAvailableMovesFrom(eq(validFieldA()), any(BoardNavigator.class))).willReturn(noMoves);

        // expect
        assertThatIllegalArgumentException()
                .isThrownBy(() -> board.move(validFieldA(), validFieldB()))
                .withMessage("Invalid move for a piece");
    }

    @Test
    @DisplayName("cannot move a piece: square is empty")
    void cannotMovePiece2() {
        // expect
        assertThatIllegalArgumentException()
                .isThrownBy(() -> board.move(emptyField, validFieldA()))
                .withMessage("Cannot make a move from an empty square!");
    }

    @Test
    @DisplayName("should show moves for piece")
    void showMovesForPiece() {
        given(piece.getAvailableMovesFrom(eq(validFieldA()), any(BoardNavigator.class))).willReturn(Set.of(validFieldB(), validFieldC()));

        final var availableMovesSet = board.getAvailableMovesFrom(validFieldA());

        BDDAssertions.then(availableMovesSet).containsOnly(validFieldB(), validFieldC());
    }

    @Test
    @DisplayName("should display no moves if field is empty")
    void shouldMovesForInvalidPiece() {
        final var availableMovesSet = board.getAvailableMovesFrom(emptyField);

        BDDAssertions.then(availableMovesSet).isEmpty();
    }
}
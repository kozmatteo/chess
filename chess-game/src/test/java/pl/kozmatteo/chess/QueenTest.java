package pl.kozmatteo.chess;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static pl.kozmatteo.chess.SquareFixture.merge;
import static pl.kozmatteo.chess.SquareFixture.validFieldA;

class QueenTest {
    private Set<Square> diagonalMoves = Set.of(new Square("A1"));
    private Set<Square> horizontalMoves = Set.of(new Square("A2"));
    private Set<Square> verticalMoves = Set.of(new Square("A3"));

    @Test
    @DisplayName("queen can move diagonally, horizontally and vertically")
    void getMoves() {
        var queen = aQueen();
        BoardNavigator boardNavigator = mock(BoardNavigator.class);
        given(boardNavigator.getDiagonalMoves(validFieldA(), aQueen().getPlayer())).willReturn(diagonalMoves);
        given(boardNavigator.getHorizontalMoves(validFieldA(), aQueen().getPlayer())).willReturn(horizontalMoves);
        given(boardNavigator.getVerticalMoves(validFieldA(), aQueen().getPlayer())).willReturn(verticalMoves);

        final var availableMovesSet = queen.getAvailableMovesFrom(validFieldA(), boardNavigator);

        BDDAssertions.then(availableMovesSet)
                     .containsExactlyInAnyOrderElementsOf(merge(diagonalMoves, horizontalMoves, verticalMoves));
    }

    private Queen aQueen() {
        return new Queen(Player.BLACK);
    }
}
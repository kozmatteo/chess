package pl.kozmatteo.chess;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static pl.kozmatteo.chess.SquareFixture.validFieldA;
import static pl.kozmatteo.chess.SquareFixture.validFieldB;
import static pl.kozmatteo.chess.SquareFixture.validFieldC;

class BishopTest {

    private final Set<Square> availableMoves = Set.of(validFieldB(), validFieldC());

    @Test
    @DisplayName("bishop can move diagonally")
    void getAvailableMovesFrom() {
        // given
        var bishop = aBishop();
        BoardNavigator boardNavigator = mock(BoardNavigator.class);
        given(boardNavigator.getDiagonalMoves(validFieldA(), bishop.getPlayer())).willReturn(availableMoves);

        // when
        final var availableMovesSet = bishop.getAvailableMovesFrom(validFieldA(), boardNavigator);

        BDDAssertions.then(availableMovesSet).containsExactlyInAnyOrderElementsOf(availableMoves);
    }

    public static Bishop aBishop() {
        return new Bishop(Player.WHITE);
    }
}
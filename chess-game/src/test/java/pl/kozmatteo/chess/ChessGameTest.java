package pl.kozmatteo.chess;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.thenIllegalStateException;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static pl.kozmatteo.chess.SquareFixture.validFieldA;
import static pl.kozmatteo.chess.SquareFixture.validFieldB;

@ExtendWith(MockitoExtension.class)
class ChessGameTest {
    private final PieceView whitePawn = new Pawn(Player.WHITE);
    private final PieceView blackPawn = new Pawn(Player.BLACK);

    @Mock
    private ChessBoard board;

    @Mock
    private ChessGameListener listener;

    @InjectMocks
    private ChessGame chessGame;

    @Test
    @DisplayName("should move piece if is turn for the color")
    void move() {
        // given
        given(board.getPieceAt(validFieldA())).willReturn(Optional.of(whitePawn));

        // when
        chessGame.move(validFieldA(), validFieldB());

        assertAll(
                () -> then(listener).should(times(1)).onMove(validFieldA(), validFieldB()),
                () -> then(board).should(times(1)).move(validFieldA(), validFieldB()),
                () -> assertTrue(chessGame.isTurnToMove(blackPawn))
        );
    }

    @Test
    @DisplayName("white starts")
    void isTurnToMove() {
        assertAll(
                () -> assertTrue(chessGame.isTurnToMove(whitePawn)),
                () -> assertFalse(chessGame.isTurnToMove(blackPawn))
        );
    }

    @Test
    @DisplayName("cannot move when it's not your turn")
    void blackCannotBegin() {
        thenIllegalStateException().isThrownBy(() -> chessGame.move(new Square("A7"), new Square("A6")))
                                   .withMessage("Cannot move piece. It's not your turn");
    }

    @Test
    @DisplayName("players move alternately")
    void moveAlternately() {
        // given
        given(board.getPieceAt(validFieldA())).willReturn(Optional.of(whitePawn));

        // when
        chessGame.move(validFieldA(), validFieldB());

        // then
        assertTrue(chessGame.isTurnToMove(blackPawn));
    }

}
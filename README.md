Work in progress.

Simple chess written in java with javafx developed using TDD.

Requires Java 11+ to run.

What's working here for now is:  
* alternate moves  
* all pieces basic movements  
* display available movements   

List of a missing features:  
* pawn can take opponent diagonally  
* piece taking  
* check  
* checkmate  
* stalemate  
* castling  
* pawn promotion  
* en passant  


### Run  
`./gradlew run`

### Testing
There's an acceptance test for GUI that can be executed:  
`./gradlew acceptanceTest`
package pl.kozmatteo.chess.app

import javafx.css.PseudoClass
import javafx.scene.input.MouseButton
import org.testfx.api.FxAssert
import org.testfx.api.FxRobot
import org.testfx.matcher.control.LabeledMatchers

import static org.hamcrest.CoreMatchers.hasItem
import static org.testfx.api.FxAssert.verifyThat
import static org.testfx.assertions.api.Assertions.assertThat

class ApplicationDriver {
    private FxRobot robot = new FxRobot()

    def layoutMatches(def layout) {
        layout.each { k, v ->
            def expectedText = v != "-" ? v : ""
            verifyThat "#${k}", LabeledMatchers.hasText(expectedText)
        }
    }

    def fieldHasColor(String coordinates, String color) {
        def field = findFieldById(coordinates).parent
        assertThat(field).hasStyle("-fx-background-color: ${color.toUpperCase()};")
    }

    private findFieldById(String coordinates) {
        robot.lookup("#${coordinates}").query()
    }

    private findField(String selector) {
        robot.lookup("${selector}").query()
    }

    void move(String fromCoordinates, String toCoordinates) {
        robot.drag(findFieldById(fromCoordinates), MouseButton.PRIMARY)
        robot.interact { robot.dropTo((findFieldById(toCoordinates))) }
    }

    void displayMovesFrom(String coordinates) {
        robot.clickOn(findFieldById(coordinates))
    }

    void assertShownMovesContain(List<String> expectedCoordinates) {
        expectedCoordinates.each {
            // todo make actually working selector
            def field = findField(".field:selected #${it}").parent
            FxAssert.verifyThat(field.pseudoClassStates, hasItem(PseudoClass.getPseudoClass("available")),
                    { sb -> sb.append("expected pseudoclass at: #").append(it) })
        }
    }
}

package pl.kozmatteo.chess.app

class ChessGameDSL {
    private ApplicationDriver driver = new ApplicationDriver()

    static ChessGameDSL aChessGame() {
        new ChessGameDSL()
    }

    static ChessGameDSL the(ChessGameDSL dsl) {
        dsl
    }

    def boardLayoutMatches(String layout) {
        driver.layoutMatches parseBoardToMap(layout)
    }

    private static parseBoardToMap(String layout) {
        def columnLetters = 'a'..'h'
        def boardSize = 0..7
        String[] rows = layout.split("\r?\n").reverse()
        def layoutAsMap = [:]
        for (rowNumber in boardSize) {
            String row = rows[rowNumber]
            def columns = row.split(" ")
            for (columnNumber in boardSize) {
                layoutAsMap["${columnLetters[columnNumber]}${rowNumber + 1}"] = columns[columnNumber]
            }
        }
        layoutAsMap
    }

    void hasWhiteSquareAtTheClosestRightHandSideForBothPlayers() {
        driver.fieldHasColor("a8", "white")
        driver.fieldHasColor("h1", "white")
    }

    void makesMove(String fromCoordinate, String toCoordinate) {
        driver.move(fromCoordinate, toCoordinate)
    }

    void hasPiece(String piece, String coordinates) {
        def map = [:]
        map[coordinates] = piece
        driver.layoutMatches(map)
    }

    void isEmpty(String coordinates) {
        hasPiece("-", coordinates)
    }

    void displayMovesFrom(String coordinates) {
        driver.displayMovesFrom(coordinates)
    }

    void movesAreShown(List<String> expectedMoves) {
        driver.assertShownMovesContain(expectedMoves)
    }
}

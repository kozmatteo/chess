package pl.kozmatteo.chess.app

import javafx.stage.Stage
import org.testfx.api.FxToolkit
import org.testfx.framework.spock.ApplicationSpec
import spock.lang.Ignore
import spock.lang.Shared

import static ChessGameDSL.the
import static pl.kozmatteo.chess.app.ChessGameDSL.aChessGame

class ChessSpec extends ApplicationSpec {

    @Shared
    ChessGameDSL chessGame

    @Override
    void init() throws Exception {
        FxToolkit.registerStage { new Stage() }
    }

    @Override
    void stop() throws Exception {
        FxToolkit.hideStage()
    }

    @Override
    void start(Stage stage) throws Exception {
        JavaFXApp.start stage
        chessGame = aChessGame()
    }

    def "board setup test"() {
        expect:
        the chessGame boardLayoutMatches """BR BKn BB BK BQ BB BKn BR
BP BP BP BP BP BP BP BP
- - - - - - - -
- - - - - - - -
- - - - - - - -
- - - - - - - -
WP WP WP WP WP WP WP WP
WR WKn WB WQ WK WB WKn WR
"""
        and:
        the chessGame hasWhiteSquareAtTheClosestRightHandSideForBothPlayers()
    }

    def "whites always move first"() {
        when:
        the chessGame makesMove "a2", "a3"

        then:
        the chessGame hasPiece "WP", "a3"
    }

    def "blacks cannot move first"() {
        when:
        the chessGame makesMove "a7", "a6"

        then:
        the chessGame isEmpty "a6"
        the chessGame hasPiece "BP", "a7"
    }

    def "players move alternately"() {
        when:
        the chessGame makesMove "a2", "a3"
        the chessGame makesMove "a7", "a6"

        then:
        the chessGame hasPiece "WP", "a3"
        the chessGame hasPiece "BP", "a6"
    }

    def "pawn cannot move by 2 after its first move"() {
        given: "white pawn is moved forward by one"
        the chessGame makesMove "a2", "a3"
        the chessGame makesMove "b7", "b5"

        when: "in the next turn white cannot move the white pawn forward by 2 squares"
        the chessGame makesMove "a3", "a5"

        then:
        the chessGame hasPiece "BP", "b5"
        the chessGame hasPiece "WP", "a3"
    }

    def "should display available moves for a selected piece"() {
        when:
        the chessGame displayMovesFrom "a2"

        then:
        the chessGame movesAreShown(["a3", "a4"])
    }

    @Ignore("work in progress")
    def "should display take on diagonal for the pawn"() {
        given:
        the chessGame makesMove "a2", "a4"
        the chessGame makesMove "b7", "b5"

        when:
        the chessGame displayMovesFrom "a4"

        then:
        the chessGame movesAreShown(["b5", "a5"])
    }

    def "should not display moves that are unavailable"() {
        given:
        the chessGame makesMove "c2", "c3"
        the chessGame makesMove "b7", "b6"
        the chessGame makesMove "d1", "b3"
        the chessGame makesMove "h7", "h6"

        when:
        the chessGame displayMovesFrom "b3"

        then:
        the chessGame movesAreShown(['a3',
                                     'a4',
                                     'b4', 'b5', 'b6',
                                     'c4', 'd5', 'e6', 'f7',
                                     'c2', 'd1'])
    }
}

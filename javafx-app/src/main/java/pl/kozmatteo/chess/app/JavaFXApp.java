package pl.kozmatteo.chess.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class JavaFXApp {
    public static void main(String[] args) {
        Application.launch(AppRunner.class, args);
    }

    public static class AppRunner extends Application {
        @Override
        public void start(Stage primaryStage) throws IOException {
           JavaFXApp.start(primaryStage);
        }
    }

    public static void start(Stage primaryStage) throws IOException {
        final var fxmlLoader = new FXMLLoader(JavaFXApp.class.getResource("/board.fxml"));
        final var scene = new Scene(fxmlLoader.load());
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

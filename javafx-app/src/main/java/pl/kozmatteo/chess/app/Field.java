package pl.kozmatteo.chess.app;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.css.PseudoClass;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Paint;
import lombok.Getter;
import pl.kozmatteo.chess.PieceView;
import pl.kozmatteo.chess.Square;

@Getter
public class Field extends BorderPane {
    private static final PseudoClass AVAILABLE_TARGET =
            PseudoClass.getPseudoClass("available");

    private final int row;
    private final int column;
    private ObjectProperty<PieceView> piece = new SimpleObjectProperty<>();

    public Field(int row, int column) {
        this.row = row;
        this.column = column;
        setStyle(String.format("-fx-background-color: %s;", isWhite() ? "WHITE" : "BLACK"));
        final var label = new Label();
        label.setId(getCoordinates().toString());
        label.setTextFill(Paint.valueOf("red"));
        setCenter(label);
        piece.addListener((observable, old, newValue) -> label.setText(renderPiece(newValue)));
        getStyleClass().add("field");
    }

    private String renderPiece(PieceView newValue) {
        return newValue != null ? newValue.getPlayer().name().charAt(0) + newValue.getSymbol() : "";
    }

    public Square getCoordinates() {
        return new Square(String.valueOf(columnNoToLetter()) + row);
    }

    private char columnNoToLetter() {
        return (char) ('A' + column - 1);
    }

    private boolean isWhite() {
        return (row + column) % 2 == 1;
    }

    public PieceView getPiece() {
        return piece.get();
    }

    public ObjectProperty<PieceView> pieceProperty() {
        return piece;
    }

    public void setPiece(PieceView piece) {
        this.piece.set(piece);
    }

    public void removePiece() {
        this.piece.set(null);
    }

    public void markAsPotentialTarget() {
        pseudoClassStateChanged(AVAILABLE_TARGET, true);
    }

    public void removeTargetMark() {
        pseudoClassStateChanged(AVAILABLE_TARGET, false);
    }
}

package pl.kozmatteo.chess.app;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import pl.kozmatteo.chess.ChessBoard;
import pl.kozmatteo.chess.ChessGame;
import pl.kozmatteo.chess.ChessGameListener;
import pl.kozmatteo.chess.Square;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import static java.util.stream.Collectors.toList;

public class BoardController implements Initializable, ChessGameListener {
    private static final int BOARD_START = 1;
    private static final int BOARD_END = 9;

    @FXML
    public GridPane gridPane;
    private ChessGame chessGame;
    private final Map<Square, Field> fieldMap = new HashMap<>();
    private final ObservableList<Field> availableMovesTargetFields = new SimpleListProperty<>(FXCollections.observableArrayList());

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int row = BOARD_START; row < BOARD_END; row++) {
            for (int column = BOARD_START; column < BOARD_END; column++) {
                final Field field = createField(row, column);
                fieldMap.put(field.getCoordinates(), field);
                gridPane.add(field, column, BOARD_END - row);
            }
        }
        newGame();
        availableMovesTargetFields.addListener(renderAvailableTargetFields());
    }

    public Field createField(int row, int column) {
        final var field = new Field(row, column);
        field.setOnDragDetected(this::tryDragPiece);
        field.setOnDragOver(BoardController::dragPieceOver);
        field.setOnDragDropped(this::dragPieceDropped);
        field.addEventHandler(MouseEvent.MOUSE_CLICKED, this::toggleShowMovesFrom);
        return field;
    }

    private void toggleShowMovesFrom(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY) {
            final var field = (Field) event.getSource();
            displayMovesFrom(field);
            event.consume();
        }
    }

    private void displayMovesFrom(Field field) {
        final var availableMovesSet = chessGame.getAvailableMovesFrom(field.getCoordinates());
        availableMovesTargetFields.setAll(availableMovesSet.stream()
                                                           .map(fieldMap::get).collect(toList()));
    }

    private void tryDragPiece(MouseEvent event) {
        final var source = event.getSource();
        if (source instanceof Field) {
            final var field = (Field) source;
            if (chessGame.isTurnToMove(field.getPiece())) {
                startMove(field);
            }
        }
        event.consume();
    }

    private void startMove(Field field) {
        final var dragboard = field.startDragAndDrop(TransferMode.MOVE);
        final var content = new ClipboardContent();
        content.putString(field.getPiece().toString());
        dragboard.setContent(content);
        displayMovesFrom(field);
    }

    private static void dragPieceOver(DragEvent event) {
        if (event.getGestureSource() != event.getGestureTarget() && event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.MOVE);
        }
        event.consume();
    }

    private void dragPieceDropped(DragEvent event) {
        final var source = (Field) event.getGestureSource();
        final var target = (Field) event.getGestureTarget();
        movePieceTo(source, target);
        event.setDropCompleted(true);
        event.consume();
    }

    private void movePieceTo(Field source, Field target) {
        chessGame.move(source.getCoordinates(), target.getCoordinates());
    }

    private ListChangeListener<Field> renderAvailableTargetFields() {
        return c -> {
            while (c.next()) {
                c.getRemoved().forEach(Field::removeTargetMark);
                c.getAddedSubList().forEach(Field::markAsPotentialTarget);
            }
        };
    }

    public void newGame() {
        this.chessGame = new ChessGame(new ChessBoard(), this);
        Platform.runLater(this::renderPieces);
    }

    private void renderPieces() {
        fieldMap.forEach(this::renderPiece);
    }

    private void renderPiece(Square coordinate, Field field) {
        chessGame.getPieceAt(coordinate).ifPresentOrElse(field::setPiece, field::removePiece);
    }

    @Override
    public void onMove(Square from, Square to) {
        renderPiece(from, fieldMap.get(from));
        renderPiece(to, fieldMap.get(to));
        availableMovesTargetFields.clear();
    }
}

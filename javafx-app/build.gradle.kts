plugins {
    groovy
    java
    application
    id("io.freefair.lombok") version "5.0.0-rc4"
    id("org.openjfx.javafxplugin") version "0.0.8"
    id("org.unbroken-dome.test-sets") version "3.0.0"
}

group = "pl.kozmatteo"
version = "1.0-SNAPSHOT"

tasks.withType<Test> {
    useJUnitPlatform()
}

javafx {
    version = "11"
    modules("javafx.controls", "javafx.fxml")
}

application {
    mainClassName = "pl.kozmatteo.chess.app.JavaFXApp"
}

testSets {
    create("acceptanceTest")
}

tasks.named("acceptanceTest") {
    dependsOn(":test")
}

dependencies {
    implementation(project(":chess-game"))
    implementation("org.codehaus.groovy:groovy-all:2.5.8")
    testImplementation("org.spockframework:spock-core:1.3-groovy-2.5")
    testImplementation("org.assertj:assertj-core:3.15.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testImplementation("org.testfx:testfx-junit5:4.0.16-alpha")
    testImplementation("org.testfx:openjfx-monocle:jdk-12.0.1+2")
    testImplementation("org.testfx:testfx-spock:4.0.16-alpha")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:5.6.0")
    testImplementation("org.mockito:mockito-core:3.+")
    testImplementation("org.mockito:mockito-junit-jupiter:3.+")
}
